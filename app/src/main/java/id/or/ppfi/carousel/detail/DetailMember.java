package id.or.ppfi.carousel.detail;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;

import id.or.ppfi.R;
import id.or.ppfi.config.AppController;
import id.or.ppfi.config.CircularNetworkImageView;

/**
 * Created by emergency on 09/10/2016.
 */

public class DetailMember extends AppCompatActivity {
    Toolbar toolbar;

    CircularNetworkImageView thumbNail;
    ImageLoader imageLoader = AppController.getInstance().getImageLoader();

    TextView textName,textJobTitle;
    TextView text1,text2,text3,text4,text5,text6,text7;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_profile);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Detail Member");
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#b71c1c")));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        thumbNail = (CircularNetworkImageView) findViewById(R.id.profile_image);
        textName = (TextView) findViewById(R.id.text_name);
        textJobTitle = (TextView) findViewById(R.id.text_job_title);

        text1 = (TextView) findViewById(R.id.text1);
        text2 = (TextView) findViewById(R.id.text2);
        text3 = (TextView) findViewById(R.id.text3);
        text4 = (TextView) findViewById(R.id.text4);
        text5 = (TextView) findViewById(R.id.text5);
        text6 = (TextView) findViewById(R.id.text6);
        text7 = (TextView) findViewById(R.id.text7);

        Intent data = getIntent();
        thumbNail.setImageUrl(data.getStringExtra( "url_image"), imageLoader);
        textName.setText(data.getStringExtra( "name"));
        textJobTitle.setText(data.getStringExtra( "job_title"));
        text1.setText("Pendidikan Terakhir : " + data.getStringExtra( "last_education"));
        text2.setText("Tempat/Tgl.Lahir : " + data.getStringExtra("birth_place" ) + "/" + data.getStringExtra( "birth_date" ));
        text3.setText("Alamat : " + data.getStringExtra( "address"));
        text4.setText("Pekerjaan : " + data.getStringExtra( "job_title"));
        text5.setText("Tempat Melatih : " + data.getStringExtra( "coaching_place"));
        text6.setText("Cabang Olahraga : " + data.getStringExtra( "sport_branch"));
        text7.setText("Pengalaman Melatih : " + data.getStringExtra( "experience"));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                goToMainActivity();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    private void goToMainActivity() {
        this.finish();
    }




}