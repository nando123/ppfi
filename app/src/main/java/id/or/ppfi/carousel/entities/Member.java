package id.or.ppfi.carousel.entities;

public class Member {
	private String member_id;
	private String name;
	private String level;
	private String experience;
	private String url_image;
	private String job_title;
	private String birth_place;
	private String birth_date;
	private String last_education;
	private String address;
	private String coaching_place;
	private String sport_branch;
	private String gender;

	public Member() {
		super();
	}

	public Member(
			String member_id,
			String name,
			String level,
			String experience,
			String url_image,
			String job_title,
			String birth_place,
			String birth_date,
			String last_education,
			String address,
			String coaching_place,
			String sport_branch,
			String gender
	) {
		this.setMember_id(member_id);
		this.setName(name);
		this.setLevel(level);
		this.setExperience(experience);
		this.setUrl_image(url_image);
		this.setJob_title(job_title);
		this.setBirth_place(birth_place);
		this.setBirth_date(birth_date);
		this.setLast_education(last_education);
		this.setAddress(address);
		this.setSport_branch(sport_branch);
	}

	public String getBirth_place() {
		return birth_place;
	}

	public void setBirth_place(String birth_place) {
		this.birth_place = birth_place;
	}

	public String getBirth_date() {
		return birth_date;
	}

	public void setBirth_date(String birth_date) {
		this.birth_date = birth_date;
	}

	public String getLast_education() {
		return last_education;
	}

	public void setLast_education(String last_education) {
		this.last_education = last_education;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCoaching_place() {
		return coaching_place;
	}

	public void setCoaching_place(String coaching_place) {
		this.coaching_place = coaching_place;
	}

	public String getSport_branch() {
		return sport_branch;
	}

	public void setSport_branch(String sport_branch) {
		this.sport_branch = sport_branch;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getJob_title() {
		return job_title;
	}

	public void setJob_title(String job_title) {
		this.job_title = job_title;
	}

	public String getMember_id() {
		return member_id;
	}

	public void setMember_id(String member_id) {
		this.member_id = member_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getExperience() {
		return experience;
	}

	public void setExperience(String experience) {
		this.experience = experience;
	}

	public String getUrl_image() {
		return url_image;
	}

	public void setUrl_image(String url_image) {
		this.url_image = url_image;
	}
}