package id.or.ppfi.carousel.menu;

import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.Toast;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import id.or.ppfi.R;
import id.or.ppfi.carousel.detail.DetailMember;
import id.or.ppfi.carousel.entities.Member;
import id.or.ppfi.carousel.listadapter.ListAdapterMember;
import id.or.ppfi.config.ServerRequest;

import static android.widget.AbsListView.OnScrollListener.SCROLL_STATE_IDLE;

/**
 * Created by nando on 15/04/2018.
 */

public class MemberActivity extends AppCompatActivity implements OnQueryTextListener {
    Toolbar toolbar;
    private ListView listView;
    private ProgressDialog progressDialog;
    private ServerRequest serverRequest;
    private List<Member> list;
    private List<Member> listPartial;
    private Member selectedList;
    ListAdapterMember adapter;
    private ListAdapterMember listViewAdapter;
    SearchView searchView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        // getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.layout_standard);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Membership");
        //
        // getSupportActionBar().setSubtitle("keanggotan pelatih fisik terregistrasi");
        //  getSupportActionBar().setIcon(R.drawable.ic_logo_siaph);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        listView = (ListView) findViewById(R.id.listview_main);
        searchView = (SearchView) findViewById(R.id.search);
        // inputSearch = (EditText) findViewById(R.id.inputSearch);


        serverRequest = new ServerRequest();
        list = new ArrayList<Member>();
        new MainActivityAsync().execute("load");






        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                adapter.getFilter().filter(newText);

                return false;
            }
        });
    }


    private void addMoreItems(){
        int size = listPartial.size() -1;
        for(int i=1;i<=8;i++){
            if((size + i) < list.size()){
                listPartial.add(list.get(size + i));
            }
        }
        listViewAdapter.notifyDataSetChanged();
        //progressBar.setVisibility(View.GONE);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                goToMainActivity();
                break;
            case R.id.home:
                goToMainActivity();
                break;
            case R.id.refresh:
                serverRequest = new ServerRequest();
                new MainActivityAsync().execute("load");
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    private void goToMainActivity() {
        this.finish();
    }

    private List<Member> processResponse(String response) {
        List<Member> list = new ArrayList<Member>();
        try {
            JSONObject jsonObj = new JSONObject(response);
            JSONArray jsonArray = jsonObj.getJSONArray("data");
            Member data = null;
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                data = new Member();
                data.setMember_id(obj.getString("member_id"));
                data.setName(obj.getString("name"));
                data.setJob_title(obj.getString("job_title"));
                data.setExperience(obj.getString("experience"));
                data.setUrl_image(obj.getString("url_image"));
                data.setBirth_place(obj.getString("birth_place"));
                data.setBirth_date(obj.getString("birth_date"));
                data.setGender(obj.getString("gender"));
                data.setLast_education(obj.getString("last_education"));
                data.setAddress(obj.getString("address"));
                data.setCoaching_place(obj.getString("coaching_place"));
                data.setSport_branch(obj.getString("sport_branch"));


                list.add(data);
            }
        } catch (JSONException e) {
        }
        return list;
    }

    private void populateListView() {
        adapter = new ListAdapterMember(MemberActivity.this, list);
        //listView.setAdapter(adapter);

        listPartial = new ArrayList<Member>(list.subList(0, 8));
        listViewAdapter = new ListAdapterMember(MemberActivity.this, listPartial);
        listView.setAdapter(listViewAdapter);


        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

                if(listView.getLastVisiblePosition() ==  listPartial.size() - 1){
                    //progressBar.setVisibility(View.VISIBLE);
                    addMoreItems();
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                //we don't need this method, so we leave it empty
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View v,
                                    int pos, long id) {
                selectedList = (Member) adapter.getItem(pos);
                Intent in = new Intent(MemberActivity.this, DetailMember.class);
                 in.putExtra("member_id", selectedList.getMember_id());
                 in.putExtra("name", selectedList.getName());
                 in.putExtra("level", selectedList.getLevel());
                 in.putExtra("experience", selectedList.getExperience());
                 in.putExtra("url_image", selectedList.getUrl_image());
                in.putExtra("birth_place", selectedList.getBirth_place());
                 in.putExtra("birth_date", selectedList.getBirth_date());
                 in.putExtra("gender", selectedList.getGender());
                in.putExtra("last_education", selectedList.getLast_education());
                in.putExtra("address", selectedList.getAddress());
                in.putExtra("job_title", selectedList.getJob_title());
                in.putExtra("coaching_place", selectedList.getCoaching_place());
                in.putExtra("sport_branch", selectedList.getSport_branch());
                startActivity(in);
            }
        });

        /*
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           int pos, long id) {
                selectedList = (Member) adapter.getItem(pos);
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(MemberActivity.this);
                alertDialog.setTitle("Tambah Data");
                alertDialog.setMessage("Data ini akan disimpan?");
                alertDialog.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dbManager.insert("Produk Hukum Daerah No : " + selectedList.getNo_produk_hukum(), selectedList.getTentang(), selectedList.getTahun_pembuatan(), selectedList.getName(),  selectedList.getFile_name_server());
                        Toast.makeText(MemberActivity.this, "Data Tersimpan...", Toast.LENGTH_SHORT).show();
                    }
                });
                alertDialog.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                alertDialog.show();
                return true;

            }
        });
        */
    }


    private class MainActivityAsync extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(MemberActivity.this);
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(true);
            progressDialog.setIndeterminate(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            /** Mengirimkan request ke server dan memproses JSON response */
            String response = serverRequest.sendGetRequest(ServerRequest.urlGetMember);
            list = processResponse(response);

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            progressDialog.dismiss();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    populateListView();
                }
            });
        }

    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        adapter.getFilter().filter(query);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }


}