package id.or.ppfi.carousel.entities;

public class News {
	private String news_id;
	private String news_title;
	private String news_content;
	private String url_image;
	private String news_preview;
	private String news_date;
	private String news_editor;

	public News() {
		super();
	}

	public News(
			String news_id,
			String news_title,
			String news_content,
			String url_image,
			String news_preview,
			String news_date,
			String news_editor
	) {
		this.setNews_id(news_id);
		this.setNews_title(news_title);
		this.setNews_content(news_content);
		this.setUrl_image(url_image);
		this.setNews_preview(news_preview);
		this.setNews_date(news_date);
		this.setNews_editor(news_editor);
}

	public String getNews_preview() {
		return news_preview;
	}

	public void setNews_preview(String news_preview) {
		this.news_preview = news_preview;
	}

	public String getNews_date() {
		return news_date;
	}

	public void setNews_date(String news_date) {
		this.news_date = news_date;
	}

	public String getNews_editor() {
		return news_editor;
	}

	public void setNews_editor(String news_editor) {
		this.news_editor = news_editor;
	}

	public String getNews_id() {
		return news_id;
	}

	public void setNews_id(String news_id) {
		this.news_id = news_id;
	}

	public String getNews_title() {
		return news_title;
	}

	public void setNews_title(String news_title) {
		this.news_title = news_title;
	}

	public String getNews_content() {
		return news_content;
	}

	public void setNews_content(String news_content) {
		this.news_content = news_content;
	}

	public String getUrl_image() {return url_image;}

	public void setUrl_image(String url_image) { this.url_image = url_image; }
}