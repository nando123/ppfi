package id.or.ppfi.carousel.listadapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.util.ArrayList;
import java.util.List;

import id.or.ppfi.R;
import id.or.ppfi.carousel.entities.News;
import id.or.ppfi.config.AppController;
import id.or.ppfi.config.CircularNetworkImageView;

/**
 * Created by emergency on 15/12/2016.
 */
public class ListAdapterNews extends BaseAdapter implements Filterable {
    private Activity activity;
    private LayoutInflater inflater;
    private List<News> list, filterd;
   ImageLoader imageLoader = AppController.getInstance().getImageLoader();

    public ListAdapterNews(Activity activity, List<News> list) {
        this.activity = activity;
        this.list = list;
        this.filterd = this.list;
    }

    @Override
    public int getCount() {
        return filterd.size();
    }

    @Override
    public Object getItem(int position) {
        return filterd.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.news_list_item,null);
        if (imageLoader == null)
            imageLoader = AppController.getInstance().getImageLoader();

       // CircularNetworkImageView thumbNail = (CircularNetworkImageView) convertView.findViewById(R.id.thumbnail);
        NetworkImageView thumbNail = (NetworkImageView) convertView.findViewById(R.id.thumbnail);


        TextView Text1 = (TextView) convertView.findViewById(R.id.text1);
        TextView Text2 = (TextView) convertView.findViewById(R.id.text2);
       TextView Text3 = (TextView) convertView.findViewById(R.id.text3);

        News data = filterd.get(position);
        Text1.setText(data.getNews_title());
        Text2.setText(data.getNews_date());
        Text3.setText(data.getNews_preview());
        thumbNail.setImageUrl(data.getUrl_image(), imageLoader);

        return convertView;

    }



    @Override
    public Filter getFilter() {
        DataFilter filter = new DataFilter();
        return filter;
    }

    /** Class filter untuk melakukan filter (pencarian) */
    private class DataFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<News> filteredData = new ArrayList<News>();
            FilterResults result = new FilterResults();
            String filterString = constraint.toString().toLowerCase();
            for (News data : list) {

            }
            result.count = filteredData.size();
            result.values = filteredData;
            return result;
        }

        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            filterd = (List<News>) results.values;
            notifyDataSetChanged();
        }

    }

}