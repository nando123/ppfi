package id.or.ppfi.carousel.menu;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView.OnQueryTextListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import id.or.ppfi.R;
import id.or.ppfi.carousel.detail.DetailMember;
import id.or.ppfi.carousel.detail.DetailNews;
import id.or.ppfi.carousel.entities.News;
import id.or.ppfi.carousel.listadapter.ListAdapterNews;
import id.or.ppfi.config.ServerRequest;

/**
 * Created by nando on 15/04/2018.
 */

public class NewsActivity extends AppCompatActivity implements OnQueryTextListener {
    Toolbar toolbar;
    private ListView listView;
    private ProgressDialog progressDialog;
    private ServerRequest serverRequest;
    private List<News> list;
    private List<News> listPartial;
    private News selectedList;
    ListAdapterNews adapter;
    private ListAdapterNews listViewAdapter;
    SearchView searchView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_standard);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("News");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        listView = (ListView) findViewById(R.id.listview_main);
        searchView = (SearchView) findViewById(R.id.search);

        serverRequest = new ServerRequest();
        list = new ArrayList<News>();
        new MainActivityAsync().execute("load");

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return false;
            }
        });
    }

    private void addMoreItems(){
        int size = listPartial.size()-1;
        for(int i=1;i<=8;i++){
            if((size + i) < list.size()){
                listPartial.add(list.get(size + i));
            }
        }
        listViewAdapter.notifyDataSetChanged();
        //progressBar.setVisibility(View.GONE);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                goToMainActivity();
                break;
            case R.id.home:
                goToMainActivity();
                break;
            case R.id.refresh:
                serverRequest = new ServerRequest();
                new MainActivityAsync().execute("load");
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    private void goToMainActivity() {
        this.finish();
    }

    private List<News> processResponse(String response) {
        List<News> list = new ArrayList<News>();
        try {
            JSONObject jsonObj = new JSONObject(response);
            JSONArray jsonArray = jsonObj.getJSONArray("data");
            News data = null;
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                data = new News();
                data.setNews_id(obj.getString("id_news"));
                data.setNews_title(obj.getString("news_title"));
                data.setNews_content(obj.getString("news_content"));
                data.setUrl_image(obj.getString("url_image"));
                data.setNews_preview(obj.getString("news_preview"));
                data.setNews_date(obj.getString("news_date"));
                data.setNews_editor(obj.getString("news_editor"));
                list.add(data);
            }
        } catch (JSONException e) {
        }
        return list;
    }

    private void populateListView() {
        adapter = new ListAdapterNews(NewsActivity.this, list);
        listPartial = new ArrayList<News>(list.subList(0, 8));
        listViewAdapter = new ListAdapterNews(NewsActivity.this, listPartial);
        listView.setAdapter(listViewAdapter);

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

                if(listView.getLastVisiblePosition() ==  listPartial.size() - 1){
                    //progressBar.setVisibility(View.VISIBLE);
                    addMoreItems();
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                //we don't need this method, so we leave it empty
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View v,
                                    int pos, long id) {
                selectedList = (News) adapter.getItem(pos);
                Intent in = new Intent(NewsActivity.this, DetailNews.class);
                    in.putExtra("news_image", selectedList.getUrl_image());
                    in.putExtra("news_title", selectedList.getNews_title());
                    in.putExtra("news_date", selectedList.getNews_date());
                    in.putExtra("news_content", selectedList.getNews_content());
                    in.putExtra("news_editor", selectedList.getNews_editor());
                startActivity(in);
            }
        });
    }

    private class MainActivityAsync extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(NewsActivity.this);
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(true);
            progressDialog.setIndeterminate(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            /** Mengirimkan request ke server dan memproses JSON response */
            String response = serverRequest.sendGetRequest(ServerRequest.urlGetNews);
            list = processResponse(response);

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            progressDialog.dismiss();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    populateListView();
                }
            });
        }

    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        adapter.getFilter().filter(query);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }


}