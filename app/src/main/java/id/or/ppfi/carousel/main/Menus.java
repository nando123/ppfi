package id.or.ppfi.carousel.main;

public class Menus {

    private String name;
    private int imageSource;

    public Menus(int imageSource, String name) {
        this.name = name;
        this.imageSource = imageSource;
    }

    public String getName() {
        return name;
    }

    public int getImageSource() {
        return imageSource;
    }
}
