package id.or.ppfi.carousel.main;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Toast;


import java.util.ArrayList;

import id.or.ppfi.R;
import id.or.ppfi.carousel.menu.MemberActivity;
import id.or.ppfi.carousel.menu.NewsActivity;
import id.or.ppfi.config.ServerRequest;
import it.moondroid.coverflow.components.ui.containers.FeatureCoverFlow;

public class MainActivity extends AppCompatActivity {

    private FeatureCoverFlow coverFlow;
    private CoverFlowAdapter adapter;
    private ArrayList<Menus> menus;


    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }


        coverFlow = (FeatureCoverFlow) findViewById(R.id.coverflow);

        settingDummyData();
        adapter = new CoverFlowAdapter(this, menus);
        coverFlow.setAdapter(adapter);
        coverFlow.setOnScrollPositionListener(onScrollListener());
        coverFlow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View v,
                                    int pos, long id) {
                //  Toast.makeText(MainActivity.this, "position: " + pos, Toast.LENGTH_SHORT).show();
                if (pos == 0){
                } else if(pos==1){

                }else if(pos==2){
                    if(isNetworkAvailable()){
                        Intent in = new Intent(MainActivity.this, MemberActivity.class);
                        startActivity(in);
                    }else{
                        Intent in = new Intent(MainActivity.this, MemberActivity.class);
                        startActivity(in);
                    }
                }else if(pos==3){
                    if(isNetworkAvailable()){
                        Intent in = new Intent(MainActivity.this, NewsActivity.class);
                        startActivity(in);
                    }else{
                        Intent in = new Intent(MainActivity.this, NewsActivity.class);
                        startActivity(in);
                    }
                }

            }
        });
    }

    private FeatureCoverFlow.OnScrollPositionListener onScrollListener() {
        return new FeatureCoverFlow.OnScrollPositionListener() {
            @Override
            public void onScrolledToPosition(int position) {
                Log.v("MainActiivty", "position: " + position);
            }

            @Override
            public void onScrolling() {
                Log.i("MainActivity", "scrolling");
            }
        };
    }

    private void settingDummyData() {
        menus = new ArrayList<>();
        menus.add(new Menus(R.mipmap.icon_strength, "ppfi.or.id"));
        menus.add(new Menus(R.mipmap.icon_licence, "ppfi.or.id"));
        //first open
        menus.add(new Menus(R.mipmap.icon_membership_3, "ppfi.or.id"));
        menus.add(new Menus(R.mipmap.icon_news, "ppfi.or.id"));
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService( CONNECTIVITY_SERVICE );
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                break;
        }
        return super.onOptionsItemSelected(item);
    }


}