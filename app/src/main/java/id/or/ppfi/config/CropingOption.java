package id.or.ppfi.config;

import android.content.Intent;
import android.graphics.drawable.Drawable;

public class CropingOption {
	public CharSequence title;
	public Drawable icon;
	public Intent appIntent;
}