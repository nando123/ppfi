package id.or.ppfi.config;

public class AppConfig {
	// Server user login url
	public static String URL_LOGIN = "http://masterdata.iamprima.com/gondar/login.php";

	public static String URL_LOGIN_AWD = "http://masterdata.iamprima.com/gondar/login.php";

	// Server user register url
	public static String URL_REGISTER = "http://masterdata.iamprima.com/gondar/register.php";

	public static String URL_REGISTER_AWD = "http://masterdata.iamprima.com/gondar/register.php";


	public static String URL_GENERAL = "http://masterdata.iamprima.com/gondar/";

	public static String URL_CHANGE_PASSWORD = "http://masterdata.iamprima.com/gondar/change_password.php";

	public static String URL_CHANGE_PROFILE = "http://masterdata.iamprima.com/gondar/change_profile.php";


}
