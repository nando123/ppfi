package id.or.ppfi.config;

public class Config {
	// File upload url (replace the ip with your server address)
	public static final String FILE_UPLOAD_URL = "http://masterdata.iamprima.com/upload/fileUpload.php";
	public static final String URL_UPLOAD_KTP = "http://masterdata.iamprima.com/uploadktp/fileUploadKtp.php";
	public static final String URL_UPLOAD_NPWP = "http://masterdata.iamprima.com/uploadnpwp/fileUploadNpwp.php";
	public static final String URL_UPLOAD_PASSPORT = "http://masterdata.iamprima.com/uploadpassport/fileUploadPassport.php";
	public static final String URL_UPLOAD_PERIOD_PLAN = "http://masterdata.iamprima.com/uploadperiodplan/fileUploadPeriodPlan.php";
	// Directory name to store captured images and videos
    public static final String IMAGE_DIRECTORY_NAME = "IAM_PRIMA_DIR";

	public static final String URL_UPLOAD_PERIODISASI = "http://masterdata.iamprima.com/uploadperiodplan/fileUploadPeriodPlan.php";
}

